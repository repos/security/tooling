<?php

/*
To install Arcanist, pick an install directory and clone the code from GitHub:
some_install_path/ $ git clone https://github.com/phacility/arcanist.git
*/

require_once 'some_install_path/arcanist/support/init/init-script.php'; 

class PhabricatorTaskSearch
{
    private $client;
    private $api_token;

    public function __construct()
    {
        $this->api_token = "api-"; #TODO: Place your Phab token here.
        $this->client = new ConduitClient('https://phabricator.wikimedia.org/');
        $this->client->setConduitToken($this->api_token);
    }

    private function retrieveTasksInRange($createdStart, $createdEnd, $closedStart, $closedEnd)
    {
        $api_parameters = [
            'constraints' => [
                'createdStart' => $createdStart,
                'createdEnd' => $createdEnd,
                'closedStart' => $closedStart,
                'closedEnd' => $closedEnd,
                'projects' => ['security'],
            ],
        ];

        $tasks = [];

        do {
            $response = $this->client->callMethodSynchronous('maniphest.search', $api_parameters);

            if (isset($response['data'])) {
                $tasks = array_merge($tasks, $response['data']);
            }

            if (isset($response['cursor']['after'])) {
                $api_parameters['after'] = $response['cursor']['after'];
            } else {
                break;
            }
        } while ($api_parameters['after'] !== null);

        return $tasks;
    }

    public function searchTasksInDateRanges($dateRanges)
    {
        $allTasks = [];

        foreach ($dateRanges as $dateRange) {
            $tasks = $this->retrieveTasksInRange(
                $dateRange['createdStart'],
                $dateRange['createdEnd'],
                $dateRange['closedStart'],
                $dateRange['closedEnd']
            );
            $allTasks = array_merge($allTasks, $tasks);
        }

        return $allTasks;
    }
}

$dateRanges = [
    [
        'createdStart' => 1609459200, // Fri Jan 01 2021 00:00:00 GMT+0000
        'createdEnd' => 1617148799,   // Tue Mar 30 2021 23:59:59 GMT+0000
        'closedStart' => 1617148799,
        'closedEnd' => 1640908800,
    ],
    // Add more date ranges as needed
];

$phabricatorTaskSearch = new PhabricatorTaskSearch();
$tasks = $phabricatorTaskSearch->searchTasksInDateRanges($dateRanges);

if (!empty($tasks)) {
    $totalCount = count($tasks);
    echo "Total count of tasks: $totalCount" . PHP_EOL;
} else {
    echo "No tasks found." . PHP_EOL;
}
